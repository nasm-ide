///-----------------------------------------------------------------
///
/// @file      NASM IDEFrm.h
/// @author    Mahmoud Jaoune
/// Created:   19/06/2011 7:40:39 PM
/// @section   DESCRIPTION
///            NASM_IDEFrm class declaration
///
///------------------------------------------------------------------

#ifndef __NASM_IDEFRM_H__
#define __NASM_IDEFRM_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/menu.h>
#include <wx/panel.h>
#include <wx/checkbox.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/stattext.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/statusbr.h>
////Header Include End

////Dialog Style Start
#undef NASM_IDEFrm_STYLE
#define NASM_IDEFrm_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class NASM_IDEFrm : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();
		void Mnufile1003Click(wxCommandEvent& event);
		
	public:
		NASM_IDEFrm(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("NASM IDE"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = NASM_IDEFrm_STYLE);
		virtual ~NASM_IDEFrm();
		void Mnunew1004Click(wxCommandEvent& event);
		void Mnuopen1005Click(wxCommandEvent& event);
		void Mnusave1006Click(wxCommandEvent& event);
		void Mnusubmenuitem71009Click(wxCommandEvent& event);
		void Mnuedit1010Click(wxCommandEvent& event);
		void Mnucut1012Click(wxCommandEvent& event);
		void Mnucopy1013Click(wxCommandEvent& event);
		void Mnupaste1014Click(wxCommandEvent& event);
		void Mnuundo1016Click(wxCommandEvent& event);
		void Mnuredo1017Click(wxCommandEvent& event);
		void Mnuassemble1019Click(wxCommandEvent& event);
		void Mnuhelp1021Click(wxCommandEvent& event);
		void Mnu51026Click(wxCommandEvent& event);
		void Mnuabout1022Click(wxCommandEvent& event);
		void Mnusmall1027Click(wxCommandEvent& event);
		void Mnumedium1028Click(wxCommandEvent& event);
		void Mnularge1029Click(wxCommandEvent& event);
		void Mnuverylarge1030Click(wxCommandEvent& event);
		void Mnuenabled1033Click(wxCommandEvent& event);
		void Mnudisable1034Click(wxCommandEvent& event);
		void Mnuenable1036Click(wxCommandEvent& event);
		void Mnudisable1037Click(wxCommandEvent& event);
		void Mnuenable1039Click(wxCommandEvent& event);
		void Mnudisable1040Click(wxCommandEvent& event);
		void Mnudisableallstyles1041Click(wxCommandEvent& event);
		void Mnusaveas1042Click(wxCommandEvent& event);
		void Mnuselectall1044Click(wxCommandEvent& event);
		void EditorUpdated(wxCommandEvent& event);
		void EditorUpdateUI(wxUpdateUIEvent& event);
		void NASM_IDEFrmActivate(wxActivateEvent& event);
		void OkasmClick(wxCommandEvent& event);
		void CancelAsmClick(wxCommandEvent& event);
		void chboxlinkClick(wxCommandEvent& event);
		void chboxlinkClick0(wxCommandEvent& event);
		void NASM_IDEFrmActivate0(wxActivateEvent& event);
		void Mnuassemble1019Click0(wxCommandEvent& event);
		void NASM_IDEFrmActivateApp(wxActivateEvent& event);
		void EditorClick(wxRichTextEvent& event);
		void NASM_IDEFrmEnterWindow(wxMouseEvent& event);
		
	private:
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxMenuBar *WxMenuBar1;
		wxPanel *pnel;
		wxStaticText *Linknote;
		wxTextCtrl *lnkbox;
		wxStaticText *Lnkst;
		wxCheckBox *chboxlink;
		wxStaticText *Assmblest;
		wxStaticText *Note2;
		wxTextCtrl *Asmopt;
		wxButton *Okasm;
		wxButton *CancelAsm;
		wxStaticText *Asmblttl;
		wxRichTextCtrl *Editor;
		wxStatusBar *WxStatusBar1;
		////GUI Control Declaration End
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_MNU_FILE_1003 = 1003,
			ID_MNU_NEW_1004 = 1004,
			ID_MNU_OPEN_1005 = 1005,
			ID_MNU_SAVE_1006 = 1006,
			ID_MNU_SAVEAS_1042 = 1042,
			ID_MNU_SUBMENUITEM7_1009 = 1009,
			ID_MNU_EDIT_1010 = 1010,
			ID_MNU_CUT_1012 = 1012,
			ID_MNU_COPY_1013 = 1013,
			ID_MNU_PASTE_1014 = 1014,
			ID_MNU_UNDO_1016 = 1016,
			ID_MNU_REDO_1017 = 1017,
			ID_MNU_SELECTALL_1044 = 1044,
			ID_MNU_FORMAT_1023 = 1023,
			ID_MNU_FONT_1025 = 1025,
			ID_MNU_5_1026 = 1026,
			ID_MNU_SMALL_1027 = 1027,
			ID_MNU_MEDIUM_1028 = 1028,
			ID_MNU_LARGE_1029 = 1029,
			ID_MNU_VERYLARGE_1030 = 1030,
			ID_MNU_STYLE_1031 = 1031,
			ID_MNU_BOLD_1032 = 1032,
			ID_MNU_ENABLED_1033 = 1033,
			ID_MNU_DISABLE_1034 = 1034,
			ID_MNU_ITALIC_1035 = 1035,
			ID_MNU_ENABLE_1036 = 1036,
			ID_MNU_DISABLE_1037 = 1037,
			ID_MNU_UNDERLINE_1038 = 1038,
			ID_MNU_ENABLE_1039 = 1039,
			ID_MNU_DISABLE_1040 = 1040,
			ID_MNU_DISABLEALLSTYLES_1041 = 1041,
			ID_MNU_PROJECT_1018 = 1018,
			ID_MNU_ASSEMBLE_1019 = 1019,
			ID_MNU_HELP_1020 = 1020,
			ID_MNU_HELP_1021 = 1021,
			ID_MNU_ABOUT_1022 = 1022,
			
			ID_PNEL = 1057,
			ID_LINKNOTE = 1056,
			ID_LNKBOX = 1055,
			ID_LNKST = 1054,
			ID_CHBOXLINK = 1053,
			ID_ASSMBLEST = 1052,
			ID_NOTE2 = 1051,
			ID_ASMOPT = 1050,
			ID_OKASM = 1049,
			ID_CANCELASM = 1048,
			ID_ASMBLTTL = 1047,
			ID_EDITOR = 1002,
			ID_WXSTATUSBAR1 = 1001,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
		
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
